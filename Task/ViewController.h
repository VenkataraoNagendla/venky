//
//  ViewController.h
//  Task
//
//  Created by Apple on 19/04/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,NSURLSessionDataDelegate,NSURLSessionDelegate>

@property (weak, nonatomic) IBOutlet UITableView *aTable;

@end

